importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

try {
	var width = PVUtil.getDouble(pvs[0]);
	var height = PVUtil.getDouble(pvs[1]);
	var min = PVUtil.getDouble(pvs[2]);
	var max = PVUtil.getDouble(pvs[3]);
	
	widget.setPropertyValue("data_width", width);
	widget.setPropertyValue("data_height", height);
	
	widget.setPropertyValue("x_axis_maximum", width);
	widget.setPropertyValue("y_axis_maximum", height);
	
	widget.setPropertyValue("minimum", min);
	widget.setPropertyValue("maximum", max);
} catch (error) {
    // ConsoleUtil.writeInfo("ERROR:" + error);
}
