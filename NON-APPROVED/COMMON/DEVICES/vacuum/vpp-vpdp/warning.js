PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;

var warningMsg  = "";
var warningCode = 0;

if (PVUtil.getLong(pvs[0]))
	warningCode = PVUtil.getLong(pvs[1]);

switch (warningCode) {
	case 99:
		warningMsg = "Pump Not Started";
		break;
	case 98:
		warningMsg = "Pump Starting Prevented by Tripped Interlock";
		break;
	case 5:
		warningMsg = "Pressure Interlock Bypassed";
		break;
	case 4:
		warningMsg = "Hardware Interlock Bypassed";
		break;
	case 3:
		warningMsg = "Software Interlock Bypassed";
		break;

	case 0:
		break;
	default:
		warningMsg = "Warning Code: " + PVUtil.getString(pvs[1]);
		org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger().severe("Unknown warning code " + pvs[1] + " : " + warningCode);
		break;
}

if (widget.getType() != "action_button")
	widget.setPropertyValue("text", warningMsg);
widget.setPropertyValue("tooltip", warningMsg);
