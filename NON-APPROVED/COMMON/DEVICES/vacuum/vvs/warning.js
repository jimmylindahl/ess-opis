PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;

var warningMsg  = "";
var warningCode = 0;

if (PVUtil.getLong(pvs[0]))
	warningCode = PVUtil.getLong(pvs[1]);

switch (warningCode) {
	case 99:
		warningMsg = "Open/Close command timeout";
		break;

	case 0:
		break;
	default:
		warningMsg = "Warning Code: " + PVUtil.getString(pvs[1]);
		org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger().severe("Unknown warning code " + pvs[1] + " : " + warningCode);
		break;
}

try {
	pvs[2].setValue(warningMsg);
} catch (err) {
	if (widget.getType() != "action_button")
		widget.setPropertyValue("text", warningMsg);
	widget.setPropertyValue("tooltip", warningMsg);
}
