<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>$(DEVICENAME)</name>
  <macros>
    <DEVICENAME>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</DEVICENAME>
  </macros>
  <width>772</width>
  <height>725</height>
  <widget type="rectangle" version="2.0.0">
    <name>Valve Background</name>
    <width>772</width>
    <height>725</height>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Valve</name>
    <text>$(DEVICENAME)</text>
    <width>772</width>
    <height>35</height>
    <font>
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color name="WHITE" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <background_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Group Control</name>
    <x>10</x>
    <y>55</y>
    <width>275</width>
    <height>150</height>
    <style>3</style>
    <widget type="rectangle" version="2.0.0">
      <name>Control Background</name>
      <width>275</width>
      <height>150</height>
      <line_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </line_color>
      <background_color>
        <color name="BLUE-GROUP-BACKGROUND" red="179" green="209" blue="209">
        </color>
      </background_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Control Header</name>
      <text>Control</text>
      <width>275</width>
      <height>30</height>
      <font>
        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <foreground_color>
        <color name="GRAY-TEXT" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <background_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </background_color>
      <transparent>false</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>StatR</name>
      <pv_name>$(DEVICENAME):StatR</pv_name>
      <x>10</x>
      <y>40</y>
      <width>255</width>
      <height>35</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>ManualCloseCmd</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Close the Valve</description>
        </action>
      </actions>
      <pv_name>$(DEVICENAME):ManualCloseCmd</pv_name>
      <text>Close</text>
      <x>10</x>
      <y>90</y>
      <width>110</width>
      <height>50</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <tooltip>Close the Valve</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>ManualOpenCmd</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Open the Valve</description>
        </action>
      </actions>
      <pv_name>$(DEVICENAME):ManualOpenCmd</pv_name>
      <text>Open</text>
      <x>155</x>
      <y>90</y>
      <width>110</width>
      <height>50</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <tooltip>Open the Valve</tooltip>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Group Error</name>
    <x>305</x>
    <y>55</y>
    <width>457</width>
    <height>150</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>Errors Background</name>
      <width>457</width>
      <height>150</height>
      <line_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </line_color>
      <background_color>
        <color name="BLUE-GROUP-BACKGROUND" red="179" green="209" blue="209">
        </color>
      </background_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Errors Header</name>
      <text>Errors / Warnings</text>
      <width>457</width>
      <height>30</height>
      <font>
        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <foreground_color>
        <color name="GRAY-TEXT" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <background_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </background_color>
      <transparent>false</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ErrorR</name>
      <pv_name>$(DEVICENAME):ErrorR</pv_name>
      <x>10</x>
      <y>40</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="LED-RED-OFF" red="110" green="101" blue="90">
        </color>
      </off_color>
      <on_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Error Msg</name>
      <text></text>
      <x>41</x>
      <y>40</y>
      <width>406</width>
      <height>25</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <foreground_color>
        <color name="RED-TEXT" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <background_color>
        <color name="RED" red="252" green="13" blue="27">
        </color>
      </background_color>
      <transparent>false</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <rules>
        <rule name="Background Color" prop_id="background_color" out_exp="false">
          <exp bool_exp="pvInt0 == false">
            <value>
              <color name="Read_Background" red="230" green="235" blue="232">
              </color>
            </value>
          </exp>
          <pv_name>$(DEVICENAME):ErrorR</pv_name>
        </rule>
      </rules>
      <scripts>
        <script file="../error.js">
          <pv_name>$(DEVICENAME):ErrorR</pv_name>
          <pv_name>$(DEVICENAME):ErrorCodeR</pv_name>
        </script>
      </scripts>
      <tooltip>N/A</tooltip>
    </widget>
    <widget type="led" version="2.0.0">
      <name>WarningR</name>
      <pv_name>$(DEVICENAME):WarningR</pv_name>
      <x>10</x>
      <y>75</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="LED-YELLOW-OFF" red="110" green="108" blue="90">
        </color>
      </off_color>
      <on_color>
        <color name="LED-YELLOW-ON" red="255" green="235" blue="17">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Warning Msg</name>
      <text></text>
      <x>41</x>
      <y>75</y>
      <width>406</width>
      <height>25</height>
      <font>
        <font name="Default Bold" family="Source Sans Pro" style="BOLD" size="16.0">
        </font>
      </font>
      <foreground_color>
        <color name="YELLOW-TEXT" red="0" green="0" blue="0">
        </color>
      </foreground_color>
      <background_color>
        <color name="YELLOW" red="252" green="242" blue="17">
        </color>
      </background_color>
      <transparent>false</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <rules>
        <rule name="Background Color" prop_id="background_color" out_exp="false">
          <exp bool_exp="pvInt0 == false">
            <value>
              <color name="Read_Background" red="230" green="235" blue="232">
              </color>
            </value>
          </exp>
          <pv_name>$(DEVICENAME):WarningR</pv_name>
        </rule>
      </rules>
      <scripts>
        <script file="../warning.js">
          <pv_name>$(DEVICENAME):WarningR</pv_name>
          <pv_name>$(DEVICENAME):WarningCodeR</pv_name>
        </script>
      </scripts>
      <tooltip>N/A</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>RstErrorsCmd</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Reset Errors and Warnings</description>
        </action>
      </actions>
      <pv_name>$(DEVICENAME):RstErrorsCmd</pv_name>
      <text>Reset Warnings and Errors</text>
      <x>247</x>
      <y>110</y>
      <width>200</width>
      <tooltip>$(actions)</tooltip>
      <show_confirm_dialog>true</show_confirm_dialog>
    </widget>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Group Counters</name>
    <x>492</x>
    <y>530</y>
    <width>270</width>
    <height>180</height>
    <style>3</style>
    <widget type="rectangle" version="2.0.0">
      <name>Counters Background</name>
      <width>270</width>
      <height>180</height>
      <line_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </line_color>
      <background_color>
        <color name="BLUE-GROUP-BACKGROUND" red="179" green="209" blue="209">
        </color>
      </background_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Counters Header</name>
      <text>Counters</text>
      <width>270</width>
      <height>30</height>
      <font>
        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <foreground_color>
        <color name="GRAY-TEXT" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <background_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </background_color>
      <transparent>false</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label RunTimeR</name>
      <text>Runtime:</text>
      <y>40</y>
      <width>110</width>
      <height>25</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>RunTimeR</name>
      <pv_name>$(DEVICENAME):RunTimeR</pv_name>
      <x>120</x>
      <y>40</y>
      <width>140</width>
      <height>25</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>$(pv_name)$(pv_value)</tooltip>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label OpenCounterR</name>
      <text>Open Counter:</text>
      <y>75</y>
      <width>110</width>
      <height>25</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>OpenCounterR</name>
      <pv_name>$(DEVICENAME):OpenCounterR</pv_name>
      <x>120</x>
      <y>75</y>
      <width>140</width>
      <height>25</height>
      <horizontal_alignment>2</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <tooltip>$(pv_name)$(pv_value)</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>ClrCountersCmd</name>
      <actions>
        <action type="write_pv">
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
          <description>Reset Counters</description>
        </action>
      </actions>
      <pv_name>$(DEVICENAME):ClrCountersCmd</pv_name>
      <text>Reset Counters</text>
      <x>110</x>
      <y>140</y>
      <width>150</width>
      <tooltip>$(actions)</tooltip>
      <show_confirm_dialog>true</show_confirm_dialog>
    </widget>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX) Interlock</name>
    <file>vac_vvs-interlocks.bob</file>
    <macros>
      <TITLE></TITLE>
    </macros>
    <x>10</x>
    <y>225</y>
    <width>520</width>
    <height>282</height>
    <resize>2</resize>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Group Status</name>
    <x>10</x>
    <y>530</y>
    <width>462</width>
    <height>180</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>Status Background</name>
      <width>462</width>
      <height>180</height>
      <line_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </line_color>
      <background_color>
        <color name="BLUE-GROUP-BACKGROUND" red="179" green="209" blue="209">
        </color>
      </background_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Status Header</name>
      <text>Status</text>
      <width>462</width>
      <height>30</height>
      <font>
        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <foreground_color>
        <color name="GRAY-TEXT" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <background_color>
        <color name="BLUE-GROUP-BORDER" red="138" green="167" blue="167">
        </color>
      </background_color>
      <transparent>false</transparent>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ClosedR</name>
      <pv_name>$(DEVICENAME):ClosedR</pv_name>
      <x>10</x>
      <y>40</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="LED-GREEN-OFF" red="90" green="110" blue="90">
        </color>
      </off_color>
      <on_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label ClosedR</name>
      <text>Closed</text>
      <x>41</x>
      <y>40</y>
      <width>180</width>
      <height>25</height>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>OpenR</name>
      <pv_name>$(DEVICENAME):OpenR</pv_name>
      <x>10</x>
      <y>75</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="LED-GREEN-OFF" red="90" green="110" blue="90">
        </color>
      </off_color>
      <on_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label OpenR</name>
      <text>Opened</text>
      <x>41</x>
      <y>75</y>
      <width>180</width>
      <height>25</height>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>UndefinedR</name>
      <pv_name>$(DEVICENAME):UndefinedR</pv_name>
      <x>10</x>
      <y>110</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="LED-BLUE-OFF" red="90" green="110" blue="110">
        </color>
      </off_color>
      <on_color>
        <color name="LED-BLUE-ON" red="81" green="232" blue="255">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label UndefinedR</name>
      <text>Undefined</text>
      <x>41</x>
      <y>110</y>
      <width>180</width>
      <height>25</height>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>OpenDQ-RB</name>
      <pv_name>$(DEVICENAME):OpenDQ-RB</pv_name>
      <x>10</x>
      <y>145</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="LED-GREEN-OFF" red="90" green="110" blue="90">
        </color>
      </off_color>
      <on_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label OpenDQ-RB</name>
      <text>OpenDQ Readback</text>
      <x>41</x>
      <y>145</y>
      <width>180</width>
      <height>25</height>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>ValidR</name>
      <pv_name>$(DEVICENAME):ValidR</pv_name>
      <x>241</x>
      <y>40</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="LED-RED-ON" red="255" green="60" blue="46">
        </color>
      </off_color>
      <on_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label ValidR</name>
      <text>Valid</text>
      <x>272</x>
      <y>40</y>
      <width>180</width>
      <height>25</height>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>InvalidCommandR</name>
      <pv_name>$(DEVICENAME):InvalidCommandR</pv_name>
      <x>241</x>
      <y>75</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="LED-ORANGE-OFF" red="110" green="105" blue="90">
        </color>
      </off_color>
      <on_color>
        <color name="LED-ORANGE-ON" red="255" green="175" blue="81">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label InvalidCommandR</name>
      <text>Invalid Command</text>
      <x>272</x>
      <y>75</y>
      <width>180</width>
      <height>25</height>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>BeamPermit-RB</name>
      <pv_name>$(DEVICENAME):BeamPermit-RB</pv_name>
      <x>241</x>
      <y>110</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="LED-GREEN-OFF" red="90" green="110" blue="90">
        </color>
      </off_color>
      <on_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label BeamPermit-RB</name>
      <text>Beam Permit DQ Feedback</text>
      <x>272</x>
      <y>110</y>
      <width>180</width>
      <height>25</height>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="led" version="2.0.0">
      <name>BeamPermitDQ-RB</name>
      <pv_name>$(DEVICENAME):BeamPermitDQ-RB</pv_name>
      <x>241</x>
      <y>145</y>
      <width>25</width>
      <height>25</height>
      <off_color>
        <color name="LED-GREEN-OFF" red="90" green="110" blue="90">
        </color>
      </off_color>
      <on_color>
        <color name="LED-GREEN-ON" red="70" green="255" blue="70">
        </color>
      </on_color>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label BeamPermitDQ-RB</name>
      <text>Beam Permit DQ</text>
      <x>272</x>
      <y>145</y>
      <width>180</width>
      <height>25</height>
      <vertical_alignment>1</vertical_alignment>
    </widget>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>OLD Faceplate</name>
    <actions>
      <action type="open_display">
        <file>vac-vvs-popup.bob</file>
        <target>standalone</target>
        <description>Open OLD Faceplate</description>
      </action>
    </actions>
    <x>610</x>
    <y>330</y>
    <height>60</height>
    <tooltip>$(actions)</tooltip>
  </widget>
</display>
