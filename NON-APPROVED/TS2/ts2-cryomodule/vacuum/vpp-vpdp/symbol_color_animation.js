PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;

var pvAccelerating   = 0;
var pvAtNominalSpeed = 0;
var pvStopped        = 0;
var pvError          = 0;

var pvSymbol         = pvs[0];

var sum     = 0;
var isValid = 0;
var colorID = 0;

var debug = widget.getEffectiveMacros().getValue("DEBUG");
if (debug) {
	debug = debug[0];
	switch (debug) {
		case '1':
		case 'Y':
		case 'y':
		case 'T':
		case 't':
			debug = true;
			break;

		default:
			debug = false;
	}
}
else
	debug = false;

if (debug)
	Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();
else {
	Logger = new Object();
	Logger.info = function() {}
	Logger.warning = function() {}
	Logger.severe = function(text) { org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger().severe(text);}
}

function log_pv(pv) {
	Logger.info(pv + ": " + PVUtil.getString(pv));
}

try {
	pvAccelerating   = 1 * PVUtil.getInt(pvs[1]);
	pvAtNominalSpeed = 2 * PVUtil.getInt(pvs[2]);
	pvStopped        = 4 * PVUtil.getInt(pvs[3]);
	pvError          = 8 * PVUtil.getInt(pvs[4]);

	sum              = pvAccelerating | pvAtNominalSpeed | pvStopped | pvError;
	isValid          = (sum & (sum - 1)) == 0 ? 1 : 0;

	log_pv(pvs[1]);
	log_pv(pvs[2]);
	log_pv(pvs[3]);
	log_pv(pvs[4]);

	if (pvError) {
		Logger.info(pvSymbol + ": ERROR");
		colorID = 4;
	} else if (isValid == 0) {
		Logger.severe(pvSymbol + ": Invalid combination");
	} else if (pvStopped) {
		Logger.info(pvSymbol + ": STOPPED");
		colorID = 3;
	} else if (pvAtNominalSpeed) {
		Logger.info(pvSymbol + ": NOMINAL-SPEED");
		colorID = 2;
	} else if (pvAccelerating) {
		Logger.info(pvSymbol + ": ACCELERATING");
		colorID = 1;
	} else
		Logger.severe(pvSymbol + ": Unknown combination:" + sum);

	if (colorID != PVUtil.getInt(pvSymbol))
		Logger.severe(pvSymbol + ": State mismatch:" + colorID + " vs " + PVUtil.getInt(pvSymbol));
} catch (err) {
	Logger.severe("NO CONNECTION: " + err);
}


//pvSymbol.write(colorID);
